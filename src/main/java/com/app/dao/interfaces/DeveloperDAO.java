package com.app.dao.interfaces;

import com.app.entities.Developer;
import java.util.List;

public interface DeveloperDAO {

    void save(Developer d);

    Developer read(int id);

    Developer update(int id, Developer d);

    void delete(int id);

    List<Developer> getAll();

}
