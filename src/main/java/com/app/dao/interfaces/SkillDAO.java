package com.app.dao.interfaces;

import com.app.entities.Skill;
import java.util.List;

public interface SkillDAO {

    void save(Skill s);

    Skill read(int id);

    Skill update(int id, Skill s);

    void delete(int id);

    List<Skill> getAll();

}
