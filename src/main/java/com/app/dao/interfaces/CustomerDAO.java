package com.app.dao.interfaces;

import com.app.entities.Customer;
import java.util.List;

public interface CustomerDAO {

    void save(Customer c);

    Customer read(int id);

    Customer update(int id, Customer c);

    void delete(int id);

    List<Customer> getAll();

}
