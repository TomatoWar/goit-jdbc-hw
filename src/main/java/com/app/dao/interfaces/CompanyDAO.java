package com.app.dao.interfaces;

import com.app.entities.Company;
import java.util.List;

public interface CompanyDAO {

    void save(Company c);

    Company read(int id);

    Company update(int id, Company c);

    void delete(int id);

    List<Company> getAll();

}
