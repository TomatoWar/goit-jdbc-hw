package com.app.dao.interfaces;

import com.app.entities.DevSkill;
import java.util.List;

public interface DevSkillDAO {

    // there are no obvious situations in which we would like to use update method

    void save(DevSkill dS);

    boolean exists(DevSkill dS); // checks if there is an entity with passed ids in db

    List<Integer> readSkills(int devId); // returns a list of ids of every skill which a developer with passed id possesses

    List<Integer> readDevs(int skillId); // returns a list of ids of every developer which possesses a skill with passed id

    void delete(DevSkill dS);

    List<DevSkill> getAll();

}
