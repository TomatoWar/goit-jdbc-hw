package com.app.dao.interfaces;

import com.app.entities.Project;
import java.util.List;

public interface ProjectDAO {

    void save(Project p);

    Project read(int id);

    Project update(int id, Project p);

    void delete(int id);

    List<Project> getAll();

}
