package com.app.dao.implementations;

import com.app.dao.interfaces.DeveloperDAO;
import com.app.entities.Developer;

import java.util.ArrayList;
import java.util.List;

public class DeveloperMemoryDAO implements DeveloperDAO {

    private List<Developer> data = new ArrayList<>();

    public List<Developer> getAll() {
        return data;
    }

    public void save(Developer p) {
        data.add(p);
    }

    public Developer read(int id) {
        return data.get(id-1);
    }

    public Developer update(int id, Developer p) {
        Developer oldDeveloper = data.get(id-1);
        data.set(id-1, p);
        return oldDeveloper;
    }

    public void delete(int id) {
        data.remove(id-1);
    }
}
