package com.app.dao.implementations;

import com.app.dao.interfaces.CompanyDAO;
import com.app.entities.Company;
import org.jetbrains.annotations.NotNull;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CompanySqlDAO extends DataSourceManager implements CompanyDAO {

    @Override
    public void save(Company c) {

        String SQL = "INSERT INTO companies VALUES(?, ?);";

        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL)) {

            System.out.println("Executing statement...");
            statement.setInt(1, c.getId());
            statement.setString(2, c.getName());

            statement.execute();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Company read(int id) {

        Company company;

        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement("SELECT * FROM companies WHERE id = ?;")) {

            System.out.println("Executing statement...");
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next())
                company = getCompany(resultSet);
            else throw new RuntimeException("Can't find company with id " + id);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return company;

    }

    @Override
    public Company update(int id, Company c) {

        Company companyToReturn = read(id);

        String SQL = "UPDATE companies SET " +
                "id = ?, name = ? " +
                "WHERE id = ?;";

        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL)) {

            System.out.println("Executing statement...");
            statement.setInt(1, c.getId());
            statement.setString(2, c.getName());
            statement.setInt(3, id);

            statement.execute();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return companyToReturn;
    }

    @Override
    public void delete(int id) {

        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement("DELETE FROM companies WHERE id = ?;")) {

            System.out.println("Executing statement...");
            statement.setInt(1, id);
            statement.execute();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Company> getAll() {

        List<Company> companies = new ArrayList<>();

        try (Connection connection = getConnection();
             Statement statement = connection.createStatement()) {
            System.out.println("Executing statement...");
            ResultSet resultSet = statement.executeQuery("SELECT * FROM  companies");

            while (resultSet.next()) {
                Company company = getCompany(resultSet);
                companies.add(company);
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return companies;
    }

    @NotNull
    private Company getCompany(ResultSet resultSet) throws SQLException {
        Company company = new Company();
        company.setId(resultSet.getInt("id"));
        company.setName(resultSet.getString("name"));
        return company;
    }

}