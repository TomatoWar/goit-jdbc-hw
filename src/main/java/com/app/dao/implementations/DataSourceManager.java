package com.app.dao.implementations;

import javax.sql.DataSource;
import java.sql.*; // Import sql package

public abstract class DataSourceManager {

    private DataSource dataSource; // factory class for getting connections

    protected Connection getConnection() throws SQLException {
        System.out.println("Creating database connection...");
        return dataSource.getConnection();
    }  // Getting connection

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

}
