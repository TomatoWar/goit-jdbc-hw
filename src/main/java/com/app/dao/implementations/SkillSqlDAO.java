package com.app.dao.implementations;

import com.app.dao.interfaces.SkillDAO;
import com.app.entities.Skill;
import org.jetbrains.annotations.NotNull;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class SkillSqlDAO extends DataSourceManager implements SkillDAO {

    @Override
    public void save(Skill s) {

        String SQL = "INSERT INTO skills VALUES(?, ?);";

        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL)) {

            System.out.println("Executing statement...");
            statement.setInt(1, s.getId());
            statement.setString(2, s.getName());

            statement.execute();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Skill read(int id) {

        Skill skill;

        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement("SELECT * FROM skills WHERE id = ?;")) {

            System.out.println("Executing statement...");
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next())
                skill = getSkill(resultSet);
            else throw new RuntimeException("Can't find skill with id " + id);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return skill;

    }

    @Override
    public Skill update(int id, Skill s) {

        Skill skillToReturn = read(id);

        String SQL = "UPDATE skills SET " +
                "id = ?, name = ? " +
                "WHERE id = ?;";

        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL)) {

            System.out.println("Executing statement...");
            statement.setInt(1, s.getId());
            statement.setString(2, s.getName());
            statement.setInt(3, id);

            statement.execute();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return skillToReturn;
    }

    @Override
    public void delete(int id) {

        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement("DELETE FROM skills WHERE id = ?;")) {

            System.out.println("Executing statement...");
            statement.setInt(1, id);
            statement.execute();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Skill> getAll() {

        List<Skill> skills = new ArrayList<>();

        try (Connection connection = getConnection();
             Statement statement = connection.createStatement()) {
            System.out.println("Executing statement...");
            ResultSet resultSet = statement.executeQuery("SELECT * FROM  skills");

            while (resultSet.next()) {
                Skill skill = getSkill(resultSet);
                skills.add(skill);
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return skills;
    }

    @NotNull
    private Skill getSkill(ResultSet resultSet) throws SQLException {
        Skill skill = new Skill();
        skill.setId(resultSet.getInt("id"));
        skill.setName(resultSet.getString("name"));
        return skill;
    }

}