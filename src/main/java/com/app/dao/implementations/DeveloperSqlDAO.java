package com.app.dao.implementations;

import com.app.dao.interfaces.DeveloperDAO;
import com.app.entities.Developer;
import org.jetbrains.annotations.NotNull;

import java.sql.*; // Import sql package
import java.util.ArrayList;
import java.util.List;

public class DeveloperSqlDAO extends DataSourceManager implements DeveloperDAO {

    public List<Developer> getAll() {
        List<Developer> developers = new ArrayList<>();

            try (Connection connection = getConnection();
                Statement statement = connection.createStatement()) {
            System.out.println("Executing statement...");
            ResultSet resultSet = statement.executeQuery("SELECT * FROM  developers");

            while (resultSet.next()) {
                Developer developer = getDeveloper(resultSet);
                developers.add(developer);
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return developers;
    }

    public void save(Developer developer) {

        String SQL = "INSERT INTO developers VALUES(?, ?, ?, ?, ?);";

        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL)) {

            System.out.println("Executing statement...");
            statement.setInt(1, developer.getId());
            statement.setString(2, developer.getName());
            statement.setInt(3, developer.getProjectId());
            statement.setInt(4, developer.getCompanyId());
            statement.setInt(5, developer.getSalary());

            statement.execute();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    public Developer read(int id) {
        Developer developer;

        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement("SELECT * FROM developers WHERE id = ?;")) {

            System.out.println("Executing statement...");
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next())
                developer = getDeveloper(resultSet);
            else throw new RuntimeException("Can't find developer with id " + id);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return developer;
    }

    public Developer update(int id, Developer developer) {

        Developer devToUpdate = read(id);

        String SQL = "UPDATE developers SET " +
                "id = ?, name = ?, project_id = ?, company_id = ?, salary = ? " +
                "WHERE id = ?;";

        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL)) {

            System.out.println("Executing statement...");
            statement.setInt(1, developer.getId());
            statement.setString(2, developer.getName());
            statement.setInt(3, developer.getProjectId());
            statement.setInt(4, developer.getCompanyId());
            statement.setInt(5, developer.getSalary());
            statement.setInt(6, id);

            statement.execute();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return devToUpdate;

    }

    public void delete(int id) {

        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement("DELETE FROM developers WHERE id = ?;")) {

            System.out.println("Executing statement...");
            statement.setInt(1, id);
            statement.execute();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    @NotNull
    private Developer getDeveloper(ResultSet resultSet) throws SQLException {
        Developer developer = new Developer();
        developer.setId(resultSet.getInt("id"));
        developer.setName(resultSet.getString("name"));
        developer.setProjectId(resultSet.getInt("project_id"));
        developer.setCompanyId(resultSet.getInt("company_id"));
        developer.setSalary(resultSet.getInt("salary"));
        return developer;
    }

}
