package com.app.dao.implementations;

import com.app.dao.interfaces.ProjectDAO;
import com.app.entities.Project;
import org.jetbrains.annotations.NotNull;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ProjectSqlDAO extends DataSourceManager implements ProjectDAO {

    @Override
    public void save(Project p) {

        String SQL = "INSERT INTO projects VALUES(?, ?, ?, ?, ?);";

        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL)) {

            System.out.println("Executing statement...");
            statement.setInt(1, p.getId());
            statement.setString(2, p.getName());
            statement.setInt(3, p.getCompanyId());
            statement.setInt(4, p.getCustomerId());
            statement.setInt(5, p.getCost());

            statement.execute();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Project read(int id) {

        Project project;

        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement("SELECT * FROM projects WHERE id = ?;")) {

            System.out.println("Executing statement...");
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next())
                project = getProject(resultSet);
            else throw new RuntimeException("Can't find project with id " + id);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return project;

    }

    @Override
    public Project update(int id, Project p) {

        Project projectToReturn = read(id);

        String SQL = "UPDATE projects SET " +
                "id = ?, name = ?, company_id = ?, customer_id =?, cost =? " +
                "WHERE id = ?;";

        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL)) {

            System.out.println("Executing statement...");
            statement.setInt(1, p.getId());
            statement.setString(2, p.getName());
            statement.setInt(3, p.getCompanyId());
            statement.setInt(4, p.getCustomerId());
            statement.setInt(5, p.getCost());
            statement.setInt(6, id);

            statement.execute();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return projectToReturn;
    }

    @Override
    public void delete(int id) {

        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement("DELETE FROM projects WHERE id = ?;")) {

            System.out.println("Executing statement...");
            statement.setInt(1, id);
            statement.execute();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Project> getAll() {

        List<Project> projects = new ArrayList<>();

        try (Connection connection = getConnection();
             Statement statement = connection.createStatement()) {
            System.out.println("Executing statement...");
            ResultSet resultSet = statement.executeQuery("SELECT * FROM  projects");

            while (resultSet.next()) {
                Project project = getProject(resultSet);
                projects.add(project);
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return projects;
    }

    @NotNull
    private Project getProject(ResultSet resultSet) throws SQLException {
        Project project = new Project();
        project.setId(resultSet.getInt("id"));
        project.setName(resultSet.getString("name"));
        project.setCompanyId(resultSet.getInt("company_id"));
        project.setCustomerId(resultSet.getInt("customer_id"));
        project.setCost(resultSet.getInt("cost"));

        return project;
    }

}