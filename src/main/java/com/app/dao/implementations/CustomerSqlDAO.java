package com.app.dao.implementations;

import com.app.dao.interfaces.CustomerDAO;
import com.app.entities.Customer;
import org.jetbrains.annotations.NotNull;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CustomerSqlDAO extends DataSourceManager implements CustomerDAO {

    @Override
    public void save(Customer c) {

        String SQL = "INSERT INTO customers VALUES(?, ?);";

        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL)) {

            System.out.println("Executing statement...");
            statement.setInt(1, c.getId());
            statement.setString(2, c.getName());

            statement.execute();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Customer read(int id) {

        Customer customer;

        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement("SELECT * FROM customers WHERE id = ?;")) {

            System.out.println("Executing statement...");
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next())
                customer = getCustomer(resultSet);
            else throw new RuntimeException("Can't find customer with id " + id);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return customer;

    }

    @Override
    public Customer update(int id, Customer c) {

        Customer customerToReturn = read(id);

        String SQL = "UPDATE customers SET " +
                "id = ?, name = ? " +
                "WHERE id = ?;";

        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL)) {

            System.out.println("Executing statement...");
            statement.setInt(1, c.getId());
            statement.setString(2, c.getName());
            statement.setInt(3, id);

            statement.execute();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return customerToReturn;
    }

    @Override
    public void delete(int id) {

        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement("DELETE FROM customers WHERE id = ?;")) {

            System.out.println("Executing statement...");
            statement.setInt(1, id);
            statement.execute();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Customer> getAll() {

        List<Customer> customers = new ArrayList<>();

        try (Connection connection = getConnection();
             Statement statement = connection.createStatement()) {
            System.out.println("Executing statement...");
            ResultSet resultSet = statement.executeQuery("SELECT * FROM  customers");

            while (resultSet.next()) {
                Customer customer = getCustomer(resultSet);
                customers.add(customer);
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return customers;
    }

    @NotNull
    private Customer getCustomer(ResultSet resultSet) throws SQLException {
        Customer customer = new Customer();
        customer.setId(resultSet.getInt("id"));
        customer.setName(resultSet.getString("name"));
        return customer;
    }

}