package com.app.dao.implementations;

import com.app.dao.interfaces.DevSkillDAO;
import com.app.entities.DevSkill;
import org.jetbrains.annotations.NotNull;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DevSkillSqlDAO extends DataSourceManager implements DevSkillDAO {

    @Override
    public void save(DevSkill dS) {

        String SQL = "INSERT INTO developers_skills VALUES(?, ?);";

        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL)) {

            System.out.println("Executing statement...");
            statement.setInt(1, dS.getDevId());
            statement.setInt(2, dS.getSkillId());

            statement.execute();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Integer> readSkills(int devId) {

        List<Integer> skillIdsList = new ArrayList<>();

        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement("SELECT skill_id FROM developers_skills WHERE dev_id = ?;")) {

            System.out.println("Executing statement...");
            statement.setInt(1, devId);
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                skillIdsList.add(resultSet.getInt("skill_id"));
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return skillIdsList;

    }

    @Override
    public List<Integer> readDevs(int skillId) {

        List<Integer> devIdList = new ArrayList<>();

        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement("SELECT dev_id FROM developers_skills WHERE skill_id = ?;")) {

            System.out.println("Executing statement...");
            statement.setInt(1, skillId);
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                devIdList.add(resultSet.getInt("dev_id"));
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return devIdList;

    }

    @Override
    public void delete(DevSkill dS) {

        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement("DELETE FROM developers_skills WHERE dev_id = ? " +
                     "AND skill_id = ?;")) {

            System.out.println("Executing statement...");
            statement.setInt(1, dS.getDevId());
            statement.setInt(2, dS.getSkillId());
            statement.execute();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<DevSkill> getAll() {

        List<DevSkill> devSkills = new ArrayList<>();

        try (Connection connection = getConnection();
             Statement statement = connection.createStatement()) {
            System.out.println("Executing statement...");
            ResultSet resultSet = statement.executeQuery("SELECT * FROM  developers_skills");

            while (resultSet.next()) {
                DevSkill devSkill = getDevSkill(resultSet);
                devSkills.add(devSkill);
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return devSkills;
    }

    @NotNull
    private DevSkill getDevSkill(ResultSet resultSet) throws SQLException {
        DevSkill devSkill = new DevSkill();
        devSkill.setDevId(resultSet.getInt("dev_id"));
        devSkill.setSkillId(resultSet.getInt("skill_id"));
        return devSkill;
    }

    @Override
    public boolean exists(DevSkill dS) {

        String SQL = "SELECT * FROM developers_skills WHERE dev_id = ? AND skill_id = ?;";

        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL)) {

            System.out.println("Executing statement...");
            statement.setInt(1, dS.getDevId());
            statement.setInt(2, dS.getSkillId());

            ResultSet resultSet = statement.executeQuery();

            return resultSet.next();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }
}