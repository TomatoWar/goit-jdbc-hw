package com.app.entities;

public class DevSkill {

    private int devId = -1;
    private int skillId = -1;

    public DevSkill(int devId, int skillId) {
        this.devId = devId;
        this.skillId = skillId;
    }

    public DevSkill() {
    }

    public int getDevId() {
        return devId;
    }

    public void setDevId(int devId) {
        this.devId = devId;
    }

    public int getSkillId() {
        return skillId;
    }

    public void setSkillId(int skillId) {
        this.skillId = skillId;
    }

    @Override
    public String toString() {
        return "DevSkill{" +
                "dev_id=" + devId +
                ", skill_id=" + skillId +
                '}';
    }
}
