package com.app.entities;

public abstract class Entity {

    protected int id = -1;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}
