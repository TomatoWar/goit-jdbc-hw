package com.app.entities;

public class Skill extends Entity {

    private String name;

    public Skill(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public Skill() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Skill{" +
                "id='" + id + '\'' +
                ", name=" + name +
                '}';
    }
}
