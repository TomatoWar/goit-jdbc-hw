package com.app.entities;

public class Project extends Entity {

    private String name;

    private int companyId = -1;

    private int customerId = -1;

    private int cost = -1;

    public Project(int id, String name, int companyId, int customerId, int cost) {
        this.id = id;
        this.name = name;
        this.companyId = companyId;
        this.customerId = customerId;
        this.cost = cost;
    }

    public Project() {
    }

    //<editor-fold desc="Getters'n'Setters">
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }
    //</editor-fold>

    @Override
    public String toString() {
        return "Project{" +
                "id=" + id +
                "name='" + name + '\'' +
                ", companyId=" + companyId +
                ", customerId=" + customerId +
                ", cost=" + cost +
                '}';
    }
}
