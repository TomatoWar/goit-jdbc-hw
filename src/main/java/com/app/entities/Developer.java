package com.app.entities;

/**
 * Created by Denis on 12.06.2017.
 */
public class Developer extends Entity {

    private String name;
    private int projectId;
    private int companyId;
    private int salary;

    //<editor-fold desc="Getters'n'Setters">

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getProjectId() {
        return projectId;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    //</editor-fold>

    public Developer() {
    }

    public Developer(int id, String name, int projectId, int companyId, int salary) {

        this.id = id;
        this.name = name;
        this.projectId = projectId;
        this.companyId = companyId;
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "Developer{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", project_id='" + projectId + '\'' +
                ", company_id='" + companyId + '\'' +
                ", salary=" + salary +
                '}';
    }
}
