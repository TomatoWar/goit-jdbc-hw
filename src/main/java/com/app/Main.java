package com.app;

import com.app.dao.implementations.*;
import com.app.dao.interfaces.*;
import com.app.entities.*;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {

    public static void main(String[] args) {

        AbstractApplicationContext context = new ClassPathXmlApplicationContext("application-context.xml");


        //<editor-fold desc="Initializing all required DAOs">
        DeveloperDAO devDao = context.getBean("developerSqlDao", DeveloperSqlDAO.class);
        SkillDAO skillDao = context.getBean("skillSqlDao", SkillSqlDAO.class);
        CompanyDAO companyDao = context.getBean("companySqlDao", CompanySqlDAO.class);
        CustomerDAO customerDao = context.getBean("customerSqlDao", CustomerSqlDAO.class);
        ProjectDAO projectDao = context.getBean("projectSqlDao", ProjectSqlDAO.class);
        DevSkillDAO devSkillDao = context.getBean("devSkillSqlDao", DevSkillSqlDAO.class);

        DeveloperMemoryDAO memDao = new DeveloperMemoryDAO();
        //</editor-fold>

        Developer testDeveloper1 = new Developer(34, "Stephan", 6, 1, 1000);
        System.out.println("Saving developer with id " + testDeveloper1.getId() + " and name " + testDeveloper1.getName() + ":");
        devDao.save(testDeveloper1);

        System.out.println("\nAdding Java SE and Java EE to recently added developer:");
        DevSkill testdevSkill1 = new DevSkill(34,1);
        DevSkill testdevSkill2 = new DevSkill(34,2);
        devSkillDao.save(testdevSkill1);
        devSkillDao.save(testdevSkill2);

        Project testProject = new Project(15, "open_ai", 4, 4, 400000);
        System.out.println("\nSaving project " + testProject);
        projectDao.save(testProject);

        System.out.println("\nNow add Stephan a new developer Monica to this project:");
        Developer testDeveloper2 = new Developer(35, "Monica", 15, 1, 1200);
        Developer testDeveloper3 = new Developer(34, "Stephan", 15, 1, 1000);
        devDao.save(testDeveloper2);
        devDao.update(34, testDeveloper3);

        System.out.println("\nAll developers:");
        devDao.getAll().forEach(System.out::println);


        // I didn't have enough time to write proper unit tests, these are hard to grasp on but they check everything:

 /*
        //<editor-fold desc="DeveloperMemoryDAO test">

        System.out.println("\nCopying data from external db to emulation:");
        devDao.getAll().forEach(memDao::save);

        System.out.println("\nINMEMORY dao:");

        System.out.println("All developers:");
        memDao.getAll().forEach(System.out::println);

        System.out.println("\nDeveloper with id 2:" );
        System.out.println(memDao.read(2));


        Developer developerJuliet = new Developer(36, "Juliet", 3, 2, 1500);

        System.out.println("----SAVE----");
        memDao.save(developerJuliet);
        System.out.println(memDao.read(memDao.getAll().size()));

        System.out.println("----READ----");
        Developer readDeveloper = memDao.read(1);
        System.out.println(readDeveloper);

        System.out.println("----UPDATE----");
        Developer developer2 = new Developer(1, "Benis", 10, 1, 8000);
        Developer oldDeveloper = memDao.update(1, developer2);
        Developer newDeveloper = memDao.read(1);
        System.out.println("old person: " + oldDeveloper);
        System.out.println("new person: " + newDeveloper);

        System.out.println("----DELETE----");
        memDao.delete(1);

        System.out.println();
        memDao.getAll().forEach(System.out::println);
        //</editor-fold>

        //<editor-fold desc="DeveloperSqlDAO test">

        System.out.println("\nDEVELOPER dao:");

        System.out.println("All developers:");
        devDao.getAll().forEach(System.out::println);

        System.out.println("\nDeveloper with id 2:" );
        System.out.println(devDao.read(2));

        Developer developer = new Developer(36, "Pasta", 8, 4, 700);
        System.out.println("\nSaving developer: ");
        devDao.save(developer);
        System.out.println("check for added developer:");
        System.out.println(devDao.read(36));

        Developer developer1 = new Developer(5, "Updater2", 3, 2, 800);
        System.out.println("\nUpdating developer: ");
        Developer firedDeveloper = devDao.update(5, developer1);
        System.out.println("Fired developer: ");
        System.out.println(firedDeveloper);
        System.out.println("check for updater developer:");
        System.out.println(devDao.read(5));

        System.out.println("\nDeleting developer: ");
        devDao.delete(7);

        System.out.println("All developers:");
        devDao.getAll().forEach(System.out::println);
        //</editor-fold>

        //<editor-fold desc="SkillSqlDAO test">

        System.out.println("\nSKILL dao:");

        System.out.println("All skills:");
        skillDao.getAll().forEach(System.out::println);

        System.out.println("\nSkill with id 2:" );
        System.out.println(skillDao.read(2));

        System.out.println("\nSaving skills: ");
        Skill skill = new Skill(10, "Http");
        skillDao.save(skill);
        System.out.println("check for added skills:");
        System.out.println(skillDao.read(10));

        System.out.println("\nUpdating a skill: ");
        Skill skill1 = new Skill(5, "Super skill");
        Skill deletedSkill = skillDao.update(5, skill1);
        System.out.println("Deleted skill: ");
        System.out.println(deletedSkill);
        System.out.println("Check for an updated skill:");
        System.out.println(skillDao.read(5));

        System.out.println("\nDeleting a skill: ");
        skillDao.delete(7);

        System.out.println("All skills:");
        skillDao.getAll().forEach(System.out::println);
        //</editor-fold>

        //<editor-fold desc="CompanySqlDAO test">

        System.out.println("\nCOMPANY dao:");

        System.out.println("All companies:");
        companyDao.getAll().forEach(System.out::println);

        System.out.println("\nCompany with id 2:" );
        System.out.println(companyDao.read(2));

        System.out.println("\nSaving companies: ");
        Company company = new Company(5, "Wut-wut company");
        companyDao.save(company);
        System.out.println("check for added companies:");
        System.out.println(companyDao.read(5));

        System.out.println("\nUpdating a company: ");
        Company company1 = new Company(2, "Super company");
        Company deletedCompany = companyDao.update(2, company1);
        System.out.println("Deleted company: ");
        System.out.println(deletedCompany);
        System.out.println("Check for an updated company:");
        System.out.println(companyDao.read(2));

        System.out.println("\nDeleting a company: ");
        companyDao.delete(1);

        System.out.println("All companies:");
        companyDao.getAll().forEach(System.out::println);
        //</editor-fold>

        //<editor-fold desc="CustomerSqlDAO test">

        System.out.println("\nCUSTOMER dao:");

        System.out.println("All customers:");
        customerDao.getAll().forEach(System.out::println);

        System.out.println("\nCustomer with id 2:" );
        System.out.println(customerDao.read(2));

        System.out.println("\nSaving customers: ");
        Customer customer = new Customer(6, "Wut-wut customer");
        customerDao.save(customer);
        System.out.println("check for added customers:");
        System.out.println(customerDao.read(6));

        System.out.println("\nUpdating a customer: ");
        Customer customer1 = new Customer(2, "Super customer");
        Customer deletedCustomer = customerDao.update(2, customer1);
        System.out.println("Deleted customer: ");
        System.out.println(deletedCustomer);
        System.out.println("Check for an updated customer: ");
        System.out.println(customerDao.read(2));

        System.out.println("\nDeleting a customer: ");
        customerDao.delete(1);

        System.out.println("All customers:");
        customerDao.getAll().forEach(System.out::println);
        //</editor-fold>

        //<editor-fold desc="ProjectSqlDAO test">

        System.out.println("\nPROJECT dao:");

        System.out.println("All projects:");
        projectDao.getAll().forEach(System.out::println);

        System.out.println("\nProject with id 2:" );
        System.out.println(projectDao.read(2));

        System.out.println("\nSaving projects: ");
        Project project = new Project(6, "Wut-wut project", 2, 2, 2000000);
        projectDao.save(project);
        System.out.println("check for added projects:");
        System.out.println(projectDao.read(6));

        System.out.println("\nUpdating a project: ");
        Project project1 = new Project(2, "Super project", 2, 3, 1500000);
        Project deletedProject = projectDao.update(2, project1);
        System.out.println("Deleted project: ");
        System.out.println(deletedProject);
        System.out.println("Check for an updated project: ");
        System.out.println(projectDao.read(2));

        System.out.println("\nDeleting a project: ");
        projectDao.delete(1);

        System.out.println("All projects:");
        projectDao.getAll().forEach(System.out::println);
        //</editor-fold>

        //<editor-fold desc="devSkillSqlDAO test">

        System.out.println("\nSKILLDEV dao:");

        System.out.println("All devSkills:");
        devSkillDao.getAll().forEach(System.out::println);

        System.out.println("\nDoes devSkill with dev_id = 3 and skill_id = 10 exist?" );
        DevSkill devSkill1 = new DevSkill(3,10);
        System.out.println(devSkillDao.exists(devSkill1));

        System.out.println("\nWhat about_id = 1 and skill_id = 1?" );
        DevSkill devSkill2 = new DevSkill(1,1);
        System.out.println(devSkillDao.exists(devSkill2));

        System.out.println("\nSaving devSkills: ");
        DevSkill devSkill3 = new DevSkill(3, 9);
        devSkillDao.save(devSkill3);
        System.out.println("\nCheck what skills developer with id=3 now has:");
        devSkillDao.readSkills(3).forEach(System.out::println);
        System.out.println("Check what developers now have skill with id 9:");
        devSkillDao.readDevs(9).forEach(System.out::println);

        System.out.println("\nNow delete devSkill with dev_id = 3 and skill_id = 9:");
        devSkillDao.delete(devSkill3);

        System.out.println("\nAll devSkills again:");
        devSkillDao.getAll().forEach(System.out::println);
        //</editor-fold>

  */
    }

}
